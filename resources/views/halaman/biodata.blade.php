@extends('layout.master')

@section('judul')
Buat Account Baru
@endsection

@section('judul1')
Sign Up Form
@endsection

    
    

@section('content')
    <form action="/welcome" method = "post">
        @csrf

        <label>First name :</label> <br>
        <input type="text" name = "fn"> <br> <br>

        <label>Last name :</label> <br>
        <input type="text" name = "ln"> <br> <br>

        <label>Gender</label> <br> <br>
        <input type="radio" name = "JK" value="Male">Male <br>
        <input type="radio" name = "JK" value="Perempuan">Female <br>
        <input type="radio" name = "JK" value="Other">Other <br> <br>

        <label>Nationality :</label> <br> <br>
        <select name="negara"> 
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select> <br> <br>

        <label>Language Spoken :</label> <br> <br>
        <input type="checkbox" name="">Bahasa Indonesia <br>
        <input type="checkbox" name="">English <br>
        <input type="checkbox" name="">Other <br> <br>

        <label>Bio</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Kirim">
    </form>
@endsection
