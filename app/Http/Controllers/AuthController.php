<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('halaman.biodata');
    }

    public function welcome(Request $request){
        $nama_depan = $request->fn;
        $nama_belakang = $request->ln;
        $jenis_kelamin = $request->JK;
        $kebangsaan = $request->negara;
        $Bio = $request->bio;
        return view ('halaman.selamat', compact('nama_depan', 'nama_belakang', 'jenis_kelamin', 'kebangsaan', 'Bio'));
    }
}
